# CGL Info

Prints information about macOS OpenGL contexts

## Usage

```
./cglinfo
CGL OpenGL Information

Global Options
==============

            CGL Version: 1.2
      Format Cache Size: 5
     Clear Format Cache: NO
       Retain Renderers: YES
      Use Error Handler: YES
        Use Build Cache: NO

Context Options
===============

         Swap Rectangle: 0,0 0x0
     Swap During VBlank: YES
    Dispatch Table Size: 7800
          Surface Order: Above
        Surface Opacity: Opaque
   Surface Backing Size: 0x0
       Surface Volatile: 0
       Current Renderer: Intel HD 5000
  GPU Vertex Processing: YES
GPU Fragment Processing: YES
           Has Drawable: NO
     MP Swaps In Flight: 1

Renderer 0
==========

                Renderer: Intel HD 5000
                  Online: YES
      Supports OffScreen: NO
     Supports FullScreen: YES
             Accelerated: YES
     Accelerated Compute: YES
                  Robust: NO
           Backing Store: YES
        Multithread Safe: YES
Supports Window Drawable: YES
   Supports Multi-Screen: NO
               Compliant: YES
            Display Mask: 0x000000ff
      Max Sample Buffers: 1
             Max Samples: 8
            Sample Alpha: YES
         Max Aux Buffers: 2
      GPU Vertex Capable: YES
    GPU Fragment Capable: YES
            Video Memory: 1610612736
       Video Memory (MB): 1536
          Texture Memory: 1610612736
     Texture Memory (MB): 1536
           Stencil Modes: 0x00000081
             Depth Modes: 0x00000c01
            Buffer Modes: monoscopic single-buffer double-buffer 
             Color Modes: ARGB1555 ARGB8888 RGBAFloat64 RGBAFloat128 

Renderer 1
==========

                Renderer: Generic Float
                  Online: YES
      Supports OffScreen: NO
     Supports FullScreen: NO
             Accelerated: NO
     Accelerated Compute: NO
                  Robust: YES
           Backing Store: YES
        Multithread Safe: YES
Supports Window Drawable: YES
   Supports Multi-Screen: YES
               Compliant: YES
            Display Mask: 0x000000ff
      Max Sample Buffers: 1
             Max Samples: 16
            Sample Alpha: YES
         Max Aux Buffers: 4
      GPU Vertex Capable: NO
    GPU Fragment Capable: NO
            Video Memory: 0
       Video Memory (MB): 0
          Texture Memory: 0
     Texture Memory (MB): 0
           Stencil Modes: 0x00000081
             Depth Modes: 0x00001001
            Buffer Modes: monoscopic single-buffer double-buffer 
             Color Modes: ARGB8888 RGBAFloat128 

```
