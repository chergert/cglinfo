/* cglinfo.c
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define GL_SILENCE_DEPRECATION

#include <OpenGL/OpenGL.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define assert_no_error(error, message)                            \
  do {                                                             \
    if (error)                                                     \
      _assert_no_error(error, __FUNCTION__, __LINE__, message);    \
  } while (0)

#define PRINT_BOLD(format, ...)                                    \
  do {                                                             \
    if (isatty (STDOUT_FILENO))                                    \
      fprintf (stdout, "\033[1m" format "\033[0m", ##__VA_ARGS__); \
    else                                                           \
      fprintf (stdout, format, ##__VA_ARGS__);                     \
  } while (0)

#define PRINT_BOOL_PROP(obj, n, k, name)                           \
  do {                                                             \
    CGLError e;                                                    \
    GLint b;                                                       \
    e = CGLDescribeRenderer (obj, n, k, &b);                       \
    assert_no_error(e, #k);                                        \
    PRINT_BOLD ("%s", name);                                       \
    fprintf (stdout, "%s\n", b ? "YES" : "NO");                    \
  } while (0)

#define PRINT_HEX_PROP(obj, n, k, name)                            \
  do {                                                             \
    CGLError e;                                                    \
    GLint v;                                                       \
    e = CGLDescribeRenderer (obj, n, k, &v);                       \
    assert_no_error(e, #k);                                        \
    PRINT_BOLD ("%s", name);                                       \
    fprintf (stdout, "0x%08x\n", v);                               \
  } while (0)

#define PRINT_INT_PROP(obj, n, k, name)                            \
  do {                                                             \
    CGLError e;                                                    \
    GLint v;                                                       \
    e = CGLDescribeRenderer (obj, n, k, &v);                       \
    assert_no_error(e, #k);                                        \
    PRINT_BOLD ("%s", name);                                       \
    fprintf (stdout, "%d\n", v);                                   \
  } while (0)

__attribute__((noreturn))
static void
_assert_no_error (CGLError    error,
                  const char *function,
                  int         lineno,
                  const char *message)
{
  fprintf (stderr,
           "%s():%d: %s: (%d): %s\n",
           function,
           lineno,
           message,
           error,
           CGLErrorString (error));
  exit (error - 10000);
}

static void
print_global_info (void)
{
  CGLError error;
  GLint format_cache_size;
  GLint clear_format_cache;
  GLint retain_renderers;
  GLint use_error_handler;
  GLint use_build_cache;
  GLint major, minor;

  CGLGetVersion (&major, &minor);

  error = CGLGetGlobalOption (kCGLGOFormatCacheSize, &format_cache_size);
  assert_no_error (error, "kCGLGOFormatCacheSize");

  error = CGLGetGlobalOption (kCGLGOClearFormatCache, &clear_format_cache);
  assert_no_error (error, "kCGLGOClearFormatCache");

  error = CGLGetGlobalOption (kCGLGORetainRenderers, &retain_renderers);
  assert_no_error (error, "kCGLGORetainRenderers");

  error = CGLGetGlobalOption (kCGLGOUseErrorHandler, &use_error_handler);
  assert_no_error (error, "kCGLGOUseErrorHandler");

  error = CGLGetGlobalOption (kCGLGOUseBuildCache, &use_build_cache);
  assert_no_error (error, "kCGLGOUseBuildCache");

  fprintf (stdout, "Global Options\n");
  fprintf (stdout, "==============\n");
  fprintf (stdout, "\n");

  PRINT_BOLD ("            CGL Version: ");
  fprintf (stdout, "%d.%d\n", major, minor);
  PRINT_BOLD ("      Format Cache Size: ");
  fprintf (stdout, "%d\n", format_cache_size);
  PRINT_BOLD ("     Clear Format Cache: ");
  fprintf (stdout, "%s\n", clear_format_cache ? "YES" : "NO");
  PRINT_BOLD ("       Retain Renderers: ");
  fprintf (stdout, "%s\n", retain_renderers ? "YES" : "NO");
  PRINT_BOLD ("      Use Error Handler: ");
  fprintf (stdout, "%s\n", use_error_handler ? "YES" : "NO");
  PRINT_BOLD ("        Use Build Cache: ");
  fprintf (stdout, "%s\n", use_build_cache ? "YES" : "NO");

  fprintf (stdout, "\n");
}

static const char *
get_renderer_name (GLint id)
{
  static char renderer_name[32];

  switch (id & kCGLRendererIDMatchingMask)
  {
  case kCGLRendererGenericID: return "Generic";
  case kCGLRendererGenericFloatID: return "Generic Float";
  case kCGLRendererAppleSWID: return "Apple Software Renderer";
  case kCGLRendererATIRage128ID: return "ATI Rage 128";
  case kCGLRendererATIRadeonID: return "ATI Radeon";
  case kCGLRendererATIRageProID: return "ATI Rage Pro";
  case kCGLRendererATIRadeon8500ID: return "ATI Radeon 8500";
  case kCGLRendererATIRadeon9700ID: return "ATI Radeon 9700";
  case kCGLRendererATIRadeonX1000ID: return "ATI Radeon X1000";
  case kCGLRendererATIRadeonX2000ID: return "ATI Radeon X2000";
  case kCGLRendererATIRadeonX3000ID: return "ATI Radeon X3000";
  case kCGLRendererATIRadeonX4000ID: return "ATI Radeon X4000";
  case kCGLRendererGeForce2MXID: return "GeForce 2 MX";
  case kCGLRendererGeForce3ID: return "GeForce 3";
  case kCGLRendererGeForceFXID: return "GeForce FX";
  case kCGLRendererGeForce8xxxID: return "GeForce 8xxx";
  case kCGLRendererGeForceID: return "GeForce";
  case kCGLRendererVTBladeXP2ID: return "VT Blade XP 2";
  case kCGLRendererIntel900ID: return "Intel 900";
  case kCGLRendererIntelX3100ID: return "Intel X3100";
  case kCGLRendererIntelHDID: return "Intel HD";
  case kCGLRendererIntelHD4000ID: return "Intel HD 4000";
  case kCGLRendererIntelHD5000ID: return "Intel HD 5000";
  case kCGLRendererMesa3DFXID: return "Mesa 3DFX";

  default:
    snprintf (renderer_name, sizeof renderer_name, "0x%08x", id & kCGLRendererIDMatchingMask);
    renderer_name[sizeof renderer_name-1] = 0;
    return renderer_name;
  }
}

static void
print_context_info (CGLContextObj context)
{
  CGLError error;
  GLint rect[4];
  GLint swap_interval;
  GLint dispatch_table_size;
  GLint surface_order;
  GLint surface_opacity = 1;
  GLint surface_backing_size[2];
  GLint surface_volatile;
  GLint current_renderer_id;
  GLint gpu_vertex_processing;
  GLint gpu_fragment_processing;
  GLint has_drawable;
  GLint mp_swaps_in_flight;

  CGLSetCurrentContext (context);

  fprintf (stdout, "Context Options\n");
  fprintf (stdout, "===============\n");
  fprintf (stdout, "\n");

  error = CGLGetParameter (context, kCGLCPSwapRectangle, rect);
  assert_no_error (error, "kCGLCPSwapRectangle");

  error = CGLGetParameter (context, kCGLCPSwapInterval, &swap_interval);
  assert_no_error (error, "kCGLCPSwapInterval");

  error = CGLGetParameter (context, kCGLCPDispatchTableSize, &dispatch_table_size);
  assert_no_error (error, "kCGLCPDispatchTableSize");

  error = CGLGetParameter (context, kCGLCPSurfaceOrder, &surface_order);
  assert_no_error (error, "kCGLCPSurfaceOrder");

  // Needs drawable assigned
  //error = CGLGetParameter (context, kCGLCPSurfaceOpacity, &surface_opacity);
  //assert_no_error (error, "kCGLCPSurfaceOpacity");

  error = CGLGetParameter (context, kCGLCPSurfaceBackingSize, surface_backing_size);
  assert_no_error (error, "kCGLCPSurfaceBackingSize");

  error = CGLGetParameter (context, kCGLCPSurfaceSurfaceVolatile, &surface_volatile);
  assert_no_error (error, "kCGLCPSurfaceSurfaceVolatile");

  error = CGLGetParameter (context, kCGLCPCurrentRendererID, &current_renderer_id);
  assert_no_error (error, "kCGLCPCurrentRendererID");

  error = CGLGetParameter (context, kCGLCPGPUVertexProcessing, &gpu_vertex_processing);
  assert_no_error (error, "kCGLCPGPUVertexProcessing");

  error = CGLGetParameter (context, kCGLCPGPUFragmentProcessing, &gpu_fragment_processing);
  assert_no_error (error, "kCGLCPGPUFragmentProcessing");

  error = CGLGetParameter (context, kCGLCPHasDrawable, &has_drawable);
  assert_no_error (error, "kCGLCPHasDrawable");

  error = CGLGetParameter (context, kCGLCPMPSwapsInFlight, &mp_swaps_in_flight);
  assert_no_error (error, "kCGLCPMPSwapsInFlight");

  PRINT_BOLD ("         Swap Rectangle: ");
  fprintf (stdout, "%d,%d %dx%d\n", rect[0], rect[1], rect[2], rect[3]);

  PRINT_BOLD ("     Swap During VBlank: ");
  fprintf (stdout, "%s\n", swap_interval ? "YES" : "NO");

  PRINT_BOLD ("    Dispatch Table Size: ");
  fprintf (stdout, "%d\n", dispatch_table_size);

  PRINT_BOLD ("          Surface Order: ");
  fprintf (stdout, "%s\n", surface_order == -1 ? "Below" : "Above");

  PRINT_BOLD ("        Surface Opacity: ");
  fprintf (stdout, "%s\n", surface_opacity == 0 ? "Transparent" : "Opaque");

  PRINT_BOLD ("   Surface Backing Size: ");
  fprintf (stdout, "%dx%d\n", surface_backing_size[0], surface_backing_size[1]);

  PRINT_BOLD ("       Surface Volatile: ");
  fprintf (stdout, "%d\n", surface_volatile);

  PRINT_BOLD ("       Current Renderer: ");
  fprintf (stdout, "%s\n", get_renderer_name (current_renderer_id));

  PRINT_BOLD ("  GPU Vertex Processing: ");
  fprintf (stdout, "%s\n", gpu_vertex_processing ? "YES" : "NO");

  PRINT_BOLD ("GPU Fragment Processing: ");
  fprintf (stdout, "%s\n", gpu_fragment_processing ? "YES" : "NO");

  PRINT_BOLD ("           Has Drawable: ");
  fprintf (stdout, "%s\n", has_drawable ? "YES" : "NO");

  PRINT_BOLD ("     MP Swaps In Flight: ");
  fprintf (stdout, "%d\n", mp_swaps_in_flight);

  fprintf (stdout, "\n");
}

static const char *
get_color_mode (GLint bit)
{
  if (0) {}
  else if (bit & kCGLRGB444Bit) return "RGB444";
  else if (bit & kCGLARGB4444Bit) return "ARGB4444";
  else if (bit & kCGLRGB444A8Bit) return "RGB444A8";
  else if (bit & kCGLRGB555Bit) return "RGB555";
  else if (bit & kCGLARGB1555Bit) return "ARGB1555";
  else if (bit & kCGLRGB555A8Bit) return "RGB555A8";
  else if (bit & kCGLRGB565Bit) return "RGB565";
  else if (bit & kCGLRGB565A8Bit) return "RGB565A8";
  else if (bit & kCGLRGB888Bit) return "RGB888";
  else if (bit & kCGLARGB8888Bit) return "ARGB8888";
  else if (bit & kCGLRGB888A8Bit) return "RGB888A8";
  else if (bit & kCGLRGB101010Bit) return "RGB101010";
  else if (bit & kCGLARGB2101010Bit) return "ARGB2101010";
  else if (bit & kCGLRGB101010_A8Bit) return "RGB101010_A8";
  else if (bit & kCGLRGB121212Bit) return "RGB121212";
  else if (bit & kCGLARGB12121212Bit) return "ARGB12121212";
  else if (bit & kCGLRGB161616Bit) return "RGB161616";
  else if (bit & kCGLRGBA16161616Bit) return "RGBA16161616";
  else if (bit & kCGLRGBFloat64Bit) return "RGBFloat64";
  else if (bit & kCGLRGBAFloat64Bit) return "RGBAFloat64";
  else if (bit & kCGLRGBFloat128Bit) return "RGBFloat128";
  else if (bit & kCGLRGBAFloat128Bit) return "RGBAFloat128";
  else if (bit & kCGLRGBFloat256Bit) return "RGBFloat256";
  else if (bit & kCGLRGBAFloat256Bit) return "RGBAFloat256";
  else return NULL;
}

static void
print_renderer_info (CGLRendererInfoObj render_obj,
                     GLint              i)
{
  CGLError error;
  GLint value;

  fprintf (stdout, "Renderer %d\n", i);
  fprintf (stdout, "==========\n");
  fprintf (stdout, "\n");

  error = CGLDescribeRenderer (render_obj, i, kCGLRPRendererID, &value);
  assert_no_error (error, "kCGLRPRendererID");
  PRINT_BOLD ("                Renderer: ");
  fprintf (stdout, "%s\n", get_renderer_name (value));

  PRINT_BOOL_PROP (render_obj, i, kCGLRPOnline,                 "                  Online: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPOffScreen,              "      Supports OffScreen: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPFullScreen,             "     Supports FullScreen: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPAccelerated,            "             Accelerated: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPAcceleratedCompute,     "     Accelerated Compute: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPRobust,                 "                  Robust: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPBackingStore,           "           Backing Store: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPMPSafe,                 "        Multithread Safe: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPWindow,                 "Supports Window Drawable: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPMultiScreen,            "   Supports Multi-Screen: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPCompliant,              "               Compliant: ");
  PRINT_HEX_PROP  (render_obj, i, kCGLRPDisplayMask,            "            Display Mask: ");
  PRINT_INT_PROP  (render_obj, i, kCGLRPMaxSampleBuffers,       "      Max Sample Buffers: ");
  PRINT_INT_PROP  (render_obj, i, kCGLRPMaxSamples,             "             Max Samples: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPSampleAlpha,            "            Sample Alpha: ");
  PRINT_INT_PROP  (render_obj, i, kCGLRPMaxAuxBuffers,          "         Max Aux Buffers: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPGPUVertProcCapable,     "      GPU Vertex Capable: ");
  PRINT_BOOL_PROP (render_obj, i, kCGLRPGPUFragProcCapable,     "    GPU Fragment Capable: ");
  PRINT_INT_PROP  (render_obj, i, kCGLRPVideoMemory,            "            Video Memory: ");
  PRINT_INT_PROP  (render_obj, i, kCGLRPVideoMemoryMegabytes,   "       Video Memory (MB): ");
  PRINT_INT_PROP  (render_obj, i, kCGLRPTextureMemory,          "          Texture Memory: ");
  PRINT_INT_PROP  (render_obj, i, kCGLRPTextureMemoryMegabytes, "     Texture Memory (MB): ");
  PRINT_HEX_PROP  (render_obj, i, kCGLRPStencilModes,           "           Stencil Modes: ");
  PRINT_HEX_PROP  (render_obj, i, kCGLRPDepthModes,             "             Depth Modes: ");

  error = CGLDescribeRenderer (render_obj, i, kCGLRPBufferModes, &value);
  assert_no_error (error, "kCGLRPBufferModes");
  PRINT_BOLD ("            Buffer Modes: ");
  if (value & kCGLMonoscopicBit)
    fprintf (stdout, "monoscopic ");
  if (value & kCGLStereoscopicBit)
    fprintf (stdout, "stereoscopic ");
  if (value & kCGLSingleBufferBit)
    fprintf (stdout, "single-buffer ");
  if (value & kCGLDoubleBufferBit)
    fprintf (stdout, "double-buffer ");
  fprintf (stdout, "\n");

  error = CGLDescribeRenderer (render_obj, i, kCGLRPColorModes, &value);
  assert_no_error (error, "kCGLRPColorModes");
  PRINT_BOLD ("             Color Modes: ");
  for (GLint nth = 0; nth < 31; nth++)
    {
      const char *mode = get_color_mode (value & (1 << nth));
      if (mode)
        fprintf (stdout, "%s ", mode);
    }
  fprintf (stdout, "\n");

  fprintf (stdout, "\n");
}

static const CGLPixelFormatAttribute attribs[] = {
  kCGLPFAOpenGLProfile, (CGLPixelFormatAttribute)kCGLOGLPVersion_3_2_Core,
  kCGLPFADoubleBuffer,
  kCGLPFAAccelerated,
  kCGLPFAColorSize, 24,
  kCGLPFAAlphaSize, 8,
  0
};

int
main (int argc,
      char *argv[])
{
  CGLPixelFormatObj pixel_format = NULL;
  CGLRendererInfoObj render_obj = NULL;
  CGLContextObj context = NULL;
  CGLError error;
  GLint n_virtual;
  GLint n_rend;

  fprintf (stdout, "CGL OpenGL Information\n");
  fprintf (stdout, "\n");

  print_global_info ();

  error = CGLChoosePixelFormat (attribs, &pixel_format, &n_virtual);
  assert_no_error (error, "CGLCreatePixelFormat");

  error = CGLCreateContext (pixel_format, NULL, &context);
  assert_no_error (error, "CGLCreateContext");

  print_context_info (context);

  error = CGLQueryRendererInfo (~(GLint)0, &render_obj, &n_rend);
  assert_no_error (error, "CGLQueryRendererInfo");

  for (GLint i = 0; i < n_rend; i++)
    print_renderer_info (render_obj, i);

  CGLDestroyRendererInfo (render_obj);

  CGLSetCurrentContext (NULL);
  CGLReleasePixelFormat (pixel_format);
  CGLReleaseContext (context);

  return 0;
}
