all: cglinfo

WARNINGS := -Wall -Werror
OPTS := -O2
DEBUG := -g

cglinfo: cglinfo.c Makefile
	$(CC) -std=gnu11 -o $@ $(WARNINGS) $(OPTS) $(DEBUG) cglinfo.c -framework OpenGL

clean:
	rm -rf cglinfo cglinfo.dSYM
